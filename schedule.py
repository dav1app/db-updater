from crontab import CronTab
import os

path = os.path.dirname(os.path.abspath(__file__))

cron = CronTab(user=True)
job = cron.new(command='cd ' + path + '; pipenv run main.py')
job.hour.every(30, 23)
cron.write()
