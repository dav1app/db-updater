from src.log import logger
from src.db import db
from src.scraper import getFilenamesFromPage
from src.requests import getDataPage, getCSV
from src.csv import csvToDataString


def update():
    jump = False

    logger.info('Connecting to database...')
    database = db()
    database.connect()

    logger.info('Getting latest date from local database...')
    date = database.getLatestDate()

    if date[0]:
        jump = True
        signature = date[0][0].strftime("%Y%m")
        logger.debug(signature)

    logger.info('Checking remote page to data scraper....')
    html = getDataPage()
    files = getFilenamesFromPage(html)

    for file in files:
        if jump:
            match = jump and file.count(signature)
        else:
            match = False
        if (not jump) or (match):
            logger.info('Downloading ' + file + '...')
            data = getCSV(file)
            if(data):
                logger.info('Downloaded' + file + '! Parsing CSV...')
                parsedData = csvToDataString(data)
                logger.info('CSV parsed! Inserting into DB...')
                database.insertData(parsedData['header'], parsedData['string'])
                logger.info('CSV inserted!')
            jump = False


update()
