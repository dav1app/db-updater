import os
import sys
import platform

from src.log import logger
from src.db import db
from src.model import get as getModel
from src.command import command


def install():

    # asdf dependencies
    logger.info('Installing dependencies using asdf...')

    if command("asdf install"):
        logger.info('asdf dependencies installed successfully!')
    else:
        logger.error('Installing dependencies failed!')
        sys.exit()

    # crontab
    logger.info('Checking if crontab is installed...')
    if command("which crontab"):
        logger.info('Crontab is installed!')
    else:
        logger.warning('Crobtab is not installed!')
        logger.info('Attempting to install it manually...')

        if platform.system() == "Linux":
            if command("sudo apt-get install crontab"):
                logger.info('Crontab installed successfully.')
            else:
                logger.error(
                    'Unable to install it automatically. Please install crontab and make it available to your $PATH.')
        else:
            logger.error(
                'Unable to install crontab in a system that is not Linux.')

    # postgres service
    logger.info('Checking database service...')
    if command("pg_ctl status"):
        logger.info('Postgres service is running!')
    else:
        logger.warning(
            'Service not running. Attempting to start service...')

        if command("pg_ctl start -w"):
            logger.info('Service started!')
        else:
            logger.warn('It seems that the database does not exists or is not pointed. Attempting to create a database in ' +
                        os.getenv("DB_DIR") + ' directory...')
            if command("pg_ctl -D " + os.getenv("DB_DIR") + " initdb; pg_ctl -W start ; pg_ctl status"):
                logger.info('Cluster directory created')
            else:
                logger.error(
                    'Unable to start postgres with pg_ctl start. Exiting...')
                sys.exit()

    # postgres check connection
    logger.info('Checking database connection...')

    database = db()
    database.connect()

    if database.connection:
        logger.info('Database connection is OK!')
    else:
        logger.warning(
            'Database connection returns an error! Attempting to create a schema via psql...')

        if command('psql -d postgres -c "CREATE SCHEMA IF NOT EXISTS dbupdater"'):
            logger.info('Schema created! Reattempting to connect')

            database = db()
            database.connect()

        else:
            sys.exit()

    # check schema
    logger.info('Checking schema...')
    database.createSchema()

    # postgres check table
    logger.info('Checking tables...')

    tableName = os.getenv("DB_TABLE")

    tables = database.checkTables(tableName)

    if tables == False:
        logger.warning('No tables were found, a table named \'' +
                       tableName + '\' will be created!')

        # Dynamic Model
        logger.info('Getting model dynamically...')
        model = getModel()
        if model:
            logger.info('Got the model!')
        else:
            logger.error('Unable to get the model!')

        logger.info('Creating ' + tableName + ' table...')

        database.createTable(model)

        if(database.checkTables(tableName)):
            logger.info('Table created successfully!')
        else:
            logger.error('Unable to create table')
            sys.exit()
    else:
        logger.info('Found table ' + tableName + '!')

    logger.info(
        'Everything seems to be working fine! Run main.py to update the database.')

    database.close()


install()
