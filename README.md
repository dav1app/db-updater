# db-updater

This is a nice updater from `http://dados.cvm.gov.br/dados/FI/DOC/INF_DIARIO/DADOS/`

## How to use

1. Clone the repository.
```bash
git clone https://gitlab.com/davimello28/db-updater.git db-updater 
cd db-updater
```

2. Set your `ENV` variables.

The `ENV` variables are important if you already have a database and a connection.

```bash
DB_USERNAME='your_username' #default to postgres
DB_PASSWORD='your_password' #default to postgres
DB_HOST='127.0.0.1' #default to 127.0.0.1
DB_PORT='5432' #default to 5432
DB_DIR="./data" #local folder for the database cluster. Defaults to /data
DB_DATABASE='postgres' #default to postgres
DB_TABLE='ficFinDiario' #default to 'ficFinDiario'
LOGURU_LEVEL='INFO' #defaults to debug, since developers are going to use it
```

3. Run your virtual env.
```bash
pipenv install
pipenv run install.py
```

4. Run the software.

```bash
# If you want to run the updater once.
pipenv run main.py

# If you want to add the crontab.
pipenv run schedule.py
```