import os
import re
from src.log import logger
from src.requests import getModel



def get():
    data = getModel()

    # Autocheck data based on the number of columns
    base = re.findall('Campo: (\w+)', data)
    
    # Remove anoying line
    result  = data.replace('-----------------------\r\n','')

    # Create clusters
    result = result.split('\r\n\r\n')

    # Remove empty clusters
    result = [i for i in result if i != '']

    # Parse cluster
    clusters = list()
    for cluster in result:
        clusters.append( cluster.split('\r\n'))

    # Parse fields
    result = []
    for cluster in clusters:
        fields = []
        dic = {}

        # Divide the ":" in the model
        for field in cluster:
            dic = {}
            unparsed = field.split(':')
            parsed = [f.strip() for f in unparsed]
            dic[parsed[0]]=parsed[1]
            fields.append(dic)

        # Transform multidimentional array in 2D dic array.
        for field in fields:
            dic.update(field)

        result.append(dic)
    return result


