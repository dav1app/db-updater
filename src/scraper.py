from bs4 import BeautifulSoup as bs

from src.log import logger


def getFilenamesFromPage(html):
    soup = bs(html, 'html.parser')

    rows = soup.findAll('table')[0].findAll('tr')

    files = []

    for row in rows:
        content = row.findAll('td')
        if len(content) > 2:
            file = content[1].findAll('a')[0].get('href')
            if file.endswith(".csv"):
                files.append(file)

    logger.debug(files)
    return files
