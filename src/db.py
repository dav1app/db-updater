import sys
import psycopg2
import os
from src.log import logger

# Set default values for database and respective envvars.
user = os.getenv("DB_USERNAME") or "postgres"
password = os.getenv("DB_PASSWORD") or "postgres"
host = os.getenv("DB_HOST") or "127.0.0.1"
port = os.getenv("DB_PORT") or "5432"
schema = os.getenv("DB_SCHEMA") or "dbupdater"
table = os.getenv("DB_TABLE").lower() or "ficfindiario"


class db:
    def __init__(self):
        self.connection = None

    # Create the connection for the database
    def connect(self):
        try:
            self.connection = psycopg2.connect(
                user=user,
                password=password,
                host=host,
                port=port,
                database='postgres')
            self.connection.autocommit = True
            self.cursor = self.connection.cursor()

        except (Exception, psycopg2.Error) as error:
            logger.error(error)
            self.connection = False

    def close(self):
        self.cursor.close()
        self.connection.close()

    # Default query method

    def query(self, query, fetch=False):
        try:
            logger.debug(query[0: 100] + "...")
            self.cursor.execute(query)
            if fetch:
                return self.cursor.fetchall()
            else:
                return True

        except (Exception, psycopg2.Error) as error:
            logger.error(error)
            return False

        self.connection.commit()

    # Create database
    def createSchema(self):
        self.query("CREATE SCHEMA IF NOT EXISTS " + schema)

    # Check if a table exists
    def checkTables(self, name):
        tables = self.query(
            "SELECT EXISTS ( SELECT FROM information_schema.tables WHERE table_name='" + name + "' AND table_schema='" + schema + "');", True)

        logger.debug(tables)

        if tables[0][0] == False:
            logger.error('No table was found')
            return False
        else:
            return True

    # Create a table
    def createTable(self, model):
        types = ",".join(getStringsFromModel(model))
        self.query("CREATE TABLE IF NOT EXISTS " +
                   schema + "." + table + " (" + types + ", PRIMARY KEY ( cnpj_fundo, dt_comptc ));")

    #
    def showAllTables(self):
        self.query("SELECT * FROM pg_catalog.pg_tables;", True)

    def insertData(self, columns, dataString):
        self.query("INSERT INTO " + schema + "." + table +
                   " (" + columns + ") VALUES " + dataString + " ON CONFLICT (cnpj_fundo, dt_comptc) DO NOTHING;")

    def getLatestDate(self):
        return self.query("SELECT DT_COMPTC FROM " + schema + "." + table +
                          " ORDER BY DT_COMPTC DESC FETCH FIRST 1 ROW ONLY;", True)

    def testData(self):
        self.query(
            "CREATE TABLE test (id serial PRIMARY KEY, num integer, data varchar);")
        self.query("INSERT INTO test (num, data) VALUES (100, 'abcdef')")
        result = self.query("SELECT * FROM test;", True)
        logger.debug(result)
        result = self.checkTables('test')
        logger.debug(result)


def getStringsFromModel(model):
    result = []

    for column in model:
        result.append('%s %s' % (column["Campo"], getExtrasString(column)))

    return result


def getExtrasString(column):

    type = column['Tipo Dados']

    if type == "numeric":
        return 'NUMERIC(%s, %s)' % (column["Precisão"], column["Scale"])
    if type == "varchar":
        return 'VARCHAR(%s)' % (column["Tamanho"])
    if type == "int":
        return 'INT'
    if type == "date":
        return 'DATE'

    logger.error('Data type not found')
