import csv
from src.log import logger


def csvToDataString(str):
    lines = str.splitlines()
    reader = csv.reader(lines, delimiter=";", quotechar='"')
    parsed_csv = list(reader)

    header = ",".join(
        map(lambda item: '"' + item.lower() + '"', parsed_csv.pop(0)))

    # Memory efficient parser
    result = ",".join(
        list(
            map(lambda row: "(" + ",".join(
                map(lambda item: "'" + item + "'", row)
            ) + ")",  parsed_csv)
        )
    )

    logger.debug(result[0:100])
    logger.debug(header)

    return {'header': header, 'string': result}
