import requests as _requests
from src.log import logger


class requests:
    def get(url):
        logger.debug('Request made to ' + url)
        req = _requests.get(url)
        if req.status_code == _requests.codes.ok:
            logger.debug('Resquest returned code ' + str(req.status_code))
            return req.text
        else:
            logger.error('Resquest returned code ' + str(req.status_code))
            return False


def getModel():
    return requests.get(
        'http://dados.cvm.gov.br/dados/FI/DOC/INF_DIARIO/META/meta_inf_diario_fi.txt')


def getDataPage():
    return requests.get(
        'http://dados.cvm.gov.br/dados/FI/DOC/INF_DIARIO/DADOS/')


def getCSV(csvName):
    return requests.get(
        'http://dados.cvm.gov.br/dados/FI/DOC/INF_DIARIO/DADOS/' + csvName)
